/* Brian Karp
 * CSe 002
 * 12/4/18
 * HW10: tic tac toe
 * this program will simulate a game of tic tac toe
 * */
import java.util.Scanner;
public class HW10{
  public static void main(String[] args){
    String[][] board = {
      {"1", "2", "3"},
      {"4", "5", "6"},
      {"7", "8", "9"}
    };
    Scanner myScanner = new Scanner(System.in);
    print(board);
    
    int turn = 1;
    do{
      if (turn == 1 || turn == 3 || turn == 5 || turn == 7 || turn == 9){
        System.out.println("Player 1, enter a location to draw an O: ");
      }
      if (turn == 2 || turn == 4 || turn == 6 || turn == 8){
        System.out.println("Player 2, enter a location to draw a X: ");
      }
    
      int temp = myScanner.nextInt();
    
      while (temp < 1 || temp > 9){
        System.out.println("Input out of range, try again: ");
        temp = myScanner.nextInt();
      }
    
      String key = String.valueOf(temp);//convert to string to compare in method
      if(temp < 10 && temp > 0){
        linearSearch(board, key, turn);
      }
    
      print(board);
      winner(board, turn); // checks winner each turn
      if(winner(board, turn) == 1){// if win, end game
        break;
      }
      turn++;
    
    }while(turn <= 9);
    
    if(turn == 10){
      System.out.println("Draw");// if not winner by turn 10, ends game
    }
    
  }//main method

  public static void print(String[][] board){//method to print array
    for(int i = 0; i < board.length; i++){  
      for(int j = 0; j < board[i].length; j++){ //prints array
        System.out.print(board[i][j] + " ");
      }
      System.out.println();
    }
  }
  
  public static void linearSearch(String[][] list, String key, int turn) { //method to search through two dimentional array and will replace each cell with X or O depending on turn count
    for(int i = 0; i < list.length; i++){
      for (int j = 0; j < list.length; j++){
        if (key.equals(list[i][j])){
          if (turn == 2 || turn == 4 || turn == 6 || turn == 8){
            list[i][j] = "X";
          }
          if (turn == 1 || turn == 3 || turn == 5 || turn == 7 || turn == 9){
            list[i][j] = "O";
          }
        }
      }
    }
  }
  
  public static int winner(String list[][], int turn){//method to print who wins
    int victory = 0;
    for(int i = 0; i < list.length; i++){
      for (int j = 0; j < list.length; j++){
        if(turn == 1 || turn == 3 || turn == 5 || turn == 7 || turn == 9){//tests win conditions for each turn separately
          if(list[0][0].equals(list[1][1]) && list[0][0].equals(list[2][2]) || list[0][0].equals(list[0][1]) && list[0][0].equals(list[0][2]) || list[0][0].equals(list[1][0]) && list[0][0].equals(list[2][0]) || list[0][1].equals(list[1][1]) && list[0][1].equals(list[2][1]) || list[0][2].equals(list[1][1]) && list[0][2].equals(list[2][0]) || list[2][2].equals(list[2][1]) && list[2][2].equals(list[2][0]) || list[0][2].equals(list[1][2]) && list[0][2].equals(list[2][2])){
            System.out.println("Player 1 wins!");
            victory = 1;
            break;
          }
        }
        if(turn == 2 || turn == 4 || turn == 6 || turn == 8){
          if(list[0][0].equals(list[1][1]) && list[0][0].equals(list[2][2]) || list[0][0].equals(list[0][1]) && list[0][0].equals(list[0][2]) || list[0][0].equals(list[1][0]) && list[0][0].equals(list[2][0]) || list[0][1].equals(list[1][1]) && list[0][1].equals(list[2][1]) || list[0][2].equals(list[1][1]) && list[0][2].equals(list[2][0]) || list[2][2].equals(list[2][1]) && list[2][2].equals(list[2][0]) || list[0][2].equals(list[1][2]) && list[0][2].equals(list[2][2])){
            System.out.println("Player 2 wins!");
            victory = 1;
            break;
          }
        }
      }
    }
    return victory;//returns victory to tell when someone wins to end game
  }
  
}