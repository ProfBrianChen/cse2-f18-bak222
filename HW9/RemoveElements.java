/* Brian Karp
CSE 002
Professor Carr
11/26/18 
*/
import java.util.Scanner;
public class RemoveElements{ // copied main method from prompt
  public static void main(String [] arg){
 Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
 String answer="";
 do{
   System.out.print("Random input 10 ints [0-9]");
   num = randomInput();
   String out = "The original array is:";
   out += listArray(num);
   System.out.println(out);
 
   System.out.print("Enter the index ");
   index = scan.nextInt();
   newArray1 = delete(num,index);
   String out1="The output array is ";
   out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
   System.out.println(out1);
 
   System.out.print("Enter the target value ");
   target = scan.nextInt();
   newArray2 = remove(num,target);
   String out2="The output array is ";
   out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
   System.out.println(out2);
    
   System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
   answer=scan.next();
 }while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
 String out="{";
 for(int j=0;j<num.length;j++){
   if(j>0){
     out+=", ";
   }
   out+=num[j];
 }
 out+="} ";
 return out;
  }
  
  public static int[] randomInput(){
    int randArray[] = new int[10];
    for (int i = 0; i < randArray.length; i++){
      randArray[i] = (int)(Math.random() * 10);
    }
    return randArray;
  }
  
  public static int[] delete(int[] list, int pos){ 
    int deleteArray[] = new int[list.length - 1];
    int j = 0;
    if(pos < list.length){
      for (int i = 0; i < list.length; i++){//new array copies list
        if(pos == i){// skips the pos when filling new array
          i++;
        }
        deleteArray[j] = list[i];
        j++;
      }
    }
    else{
      System.out.println("position out of bounds of array");
    }
    return deleteArray;
  }
  
  public static int[] remove(int[] list, int target){
    int count = 0;
    for(int i = 0; i < list.length; i++){
      if(target == list[i]){//establishes count to decide length of new array
        count++;
      }
    }
    int removeArray[] = new int[list.length - count - 1];
    int k = 0;
    for (int j = 0; j < list.length - 1; j++){//new array copies list
      if(target != list[j]){ //if target does not match, copy into new array
        removeArray[k] = list[j];
        k++;
      }  
    } 
    return removeArray;
  }
  
}

