/* Brian Karp
CSE 002
Professor Carr
11/26/18
CSE2Linear.java will 
*/
import java.util.Scanner;
import java.util.Random;
public class CSE2Linear{
  public static void main(String[] args){
    System.out.println("Enter 15 ascending ints for final grades in CSE2: ");
    Scanner myScanner = new Scanner(System.in);
    
    int[] grades = new int[15];
    Boolean correctInt = myScanner.hasNextInt(); // sets boolean value to check if the input is an integer or not 
    
    for(int i = 0; i < grades.length; i++){
      while (correctInt == false) { // if input not an integer 
        System.out.println("Not an integer grade. Try again: "); 
        correctInt = myScanner.hasNextInt(); // sets boolean value for loop to recheck
        myScanner.next(); // waits for new user input 
      }
      while (grades[i] < 0 || grades[i] > 100) { // if input not in range
       System.out.println("Input out of range. Try again: "); 
       myScanner.next(); // waits for new user input 
      }
      if(correctInt == true){
        grades[i] = myScanner.nextInt();
        correctInt = myScanner.hasNextInt();
      }
      if(i > 1){
        while (grades[i] < grades[i - 1]) { // if input not bigger than last
          System.out.println("New input smaller than last. Try again: "); 
          grades[i] = myScanner.nextInt();
        }
      } 
    }
    
    print(grades);

    System.out.println("Enter a grade to search for: "); // user input for searchable integer
    myScanner.nextInt();
    int search = myScanner.nextInt();
    binary(grades, search);//binary search
    
    scramble(grades); // scramble
    print(grades); //print
    System.out.println("Enter a grade to search for: "); // user input for searchable integer
    search = myScanner.nextInt();
    linear(grades, search);
  }
  
    public static void binary(int[] grades, int search){// binary search   
      int iterations = 0;
      int low = 0;
      int high = 14;
      int mid;
    
      while (high >= low) { 
        mid = (high + low) / 2;
        if (grades[mid] < search) {
          low = mid + 1;
          iterations++;
        } 
        else if (grades[mid] > search) {
          high = mid - 1;
          iterations++;
        } 
        else {
          System.out.println("Integer could not be found. ");
          break;
        }
        System.out.println(search + " was found in " + iterations + " iterations.");
      }
    }
    
    public static void scramble(int[] grades){
      Random rand = new Random(); // random number gen
      System.out.println("Scrambled");
      for (int i = 0; i < grades.length; i++) { // loop each int into random spot
        int randomPosition = rand.nextInt(grades.length);
        int temp = grades[i]; // temp var to hold place
        grades[i] = grades[randomPosition]; // random position move
        grades[randomPosition] = temp; // into temp varable again
      }
    }
    
    public static void print(int[] grades){
      for(int i = 0; i < grades.length; i++){ //prints array
        System.out.print(grades[i] + " ");
      }
      System.out.println();
    }
    
    
    public static void linear(int[] grades, int search){
      int iterations = 1;
      for(int i = 0; i < grades.length; i++){
        if(search == grades[i]){
          System.out.println(search + " was found in " + iterations + " iterations.");
        }
        iterations++;
      }
    }
}  