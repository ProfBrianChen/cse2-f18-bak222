/* 
Brian Karp   9/14/18
lab03        CSE 002

  Check will add the desired percentage tip and determine how much each person needs to pay to cover the whole check
*/
import java.util.Scanner;
//above imports scanner 

public class Check{
    	// main method required for every Java program
   	public static void main(String[] args) {
      
Scanner myScanner = new Scanner ( System.in ); 
//Accepts the input and declares Scanner 

System.out.print("Enter the original cost of the check in the form xx.xx: "); 
//prints prompt to user to input check amount 
  double checkCost = myScanner.nextDouble (); 
//accepts user input of check amount 

System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx) "); 
//Prompts user to input tip percentage 
  double tipPercent = myScanner.nextDouble(); 
//Accepts user input of tip percentage 
  tipPercent /= 100; 
//We want to convert the percentage into a decimal value

System.out.print("Enter the number of people who went out to dinner: "); 
//Prompts user to input number of people who are splitting the check
  int numPeople = myScanner.nextInt(); 
//Accepts user input of number of people who are splitting the check

double totalCost; 
double costPerPerson; //Delcares total cost and total cost per person as double variables 
int dollars, //whole dollar amount of cost
      dimes, pennies; //for storing digits 
                      //to the right of the dicimal point
                      //for the cost
totalCost = checkCost * (1 + tipPercent);
costPerPerson = totalCost / numPeople;
//get the whole amount, dropping decimal fraction
dollars = (int)costPerPerson; 
//casts dollar cost per person to integer 
dimes = (int)(costPerPerson * 10) % 10; 
//casts dimes cost per person to integer 
pennies = (int)(costPerPerson * 100) % 10; 
//casts pennies cost per person to integer 
System.out.println("Each person in the group owes " + dollars + '.' + dimes + pennies);
                   //prints out how much each person needs to pay

    } //end of main method 
} //end of class 