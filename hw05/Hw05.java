// Brian Karp   CSE 002
// 10/9/18      Hw05
/* this program will calculate probabilities of Four-of-a-kind, Three-of-a-kind, Two-Pair, and One-pair hands in a game of poker
*/

import java.util.Scanner; // import scanner class 

public class Hw05{
  public static void main(String[]args){ //java main method
    
    Scanner myScanner = new Scanner(System.in);
    
    int numHands = 0; // declares number of hands variable default value of 0
    
    System.out.println("Enter how many hands you want to draw: "); // prompts user to input
    boolean correctInt = myScanner.hasNextInt(); // assigns boolean to check if input is integer 
    if (correctInt == true){ // if input is integer 
        numHands = myScanner.nextInt(); // assigns new value to number of hands 
    }
    while (correctInt == false){
      System.out.println("Number of hands not an integer. Try again: "); // Prompts user to try again 
      myScanner.next(); // waits for new user input
      correctInt = myScanner.hasNextInt(); // prepares to recheck condition for next if statement 
          if (correctInt == true){ // if input is integer 
             numHands = myScanner.nextInt(); // assigns new value to number of hands 
    }
  }
    // Below declares and initilzes integeres of cards 1-5 
  int card1 = 0;
  int card2 = 0;  
  int card3 = 0;     
  int card4 = 0;
  int card5 = 0;
    // Below declares and initializes doubles of how many times each type of hand occurs
    // (These are doubles so we don't have to cast them later when calculating probability)
  double occur4Kind = 0.0;
  double occur3Kind = 0.0;
  double occur2Pair = 0.0;
  double occur1Pair = 0.0;
     
    // declares i as sentinel and sets to 1, loop until i=number of hands (input from user), increment i after each iteration
    for(int i = 1; i <= numHands; i++){ 
      card1 = (int)(Math.random()*(52))+1; // Sets each card 1-5 to a random number from 1-52
      card2 = (int)(Math.random()*(52))+1;  
      card3 = (int)(Math.random()*(52))+1;     
      card4 = (int)(Math.random()*(52))+1;
      card5 = (int)(Math.random()*(52))+1;
      while (card2 == card1){ // loop if card1 = card2, change card2 to another random number; prevents repeated cards
        myScanner.nextLine();
        card2 = (int)(Math.random()*(52)+1);
      }
      while (card3 == card2){ // loop if card2 = card3, change card3 to another random number; prevents repeated cards
        myScanner.nextLine();
        card3 = (int)(Math.random()*(52)+1); 
      }
      while (card4 == card3){ // loop if card3 = card4, change card4 to another random number; prevents repeated cards
        myScanner.nextLine();
        card4 = (int)(Math.random()*(52)+1);
      }
      while (card5 == card4){// loop if card4 = card5, change card5 to another random number; prevents repeated cards
        myScanner.nextLine();
        card5 = (int)(Math.random()*(52)+1);
      }
      
      // Below turns each card (random number) to number from 1-13
      card5 = card5 % 13 + 1;
      card4 = card4 % 13 + 1;
      card3 = card3 % 13 + 1;
      card2 = card2 % 13 + 1;
      card1 = card1 % 13 + 1;
      
      if (card1 == card2 && card1 == card3 && card1 == card4 || card1 == card3 && card1 == card4 && card1 == card5 || card1 == card2 && card1 == card4 && card1 == card5 || card1 == card2 && card1 == card3 && card1 == card5 || card2 == card3 && card2 == card4 && card2 == card5 || card2 == card3 && card2 == card4 && card2 == card1 || card2 == card1 && card2 == card4 && card2 == card5 || card2 == card3 && card2 == card1 && card2 == card5 || card3 == card4 && card3 == card5 && card3 == card1 || card3 == card4 && card3 == card5 && card3 == card2 || card3 == card2 && card3 == card5 && card3 == card1 || card3 == card4 && card3 == card2 && card3 == card1 || card4 == card5 && card4 == card1 && card4 == card2 || card4 == card5 && card4 == card1 && card4 == card3 || card4 == card5 && card4 == card3 && card4 == card2 || card4 == card3 && card4 == card1 && card4 == card2 || card5 == card1 && card5 == card2 && card5 == card3 || card5 == card4 && card5 == card2 && card5 == card3 || card5 == card1 && card5 == card4 && card5 == card3 || card5 == card1 && card5 == card2 && card5 == card4){
        occur4Kind++; // if four cards equal each other, increment four of a kind +1
      } 
      if (card1 == card2 && card1 == card3 || card1 == card2 && card1 == card4 || card1 == card2 && card1 == card5 || card1 == card3 && card1 == card4 || card1 == card3 && card1 == card5 || card1 == card4 && card1 == card5 || card2 == card3 && card2 == card4 || card2 == card3 && card2 == card5 || card2 == card4 && card2 == card5 || card3 == card4 && card3 == card5){
        occur3Kind++; // if three cards equal each other, increment three of a kind +1
      }
      if (card1 == card2 && card3 == card4 || card1 == card2 && card3 == card5 || card1 == card2 && card4 == card5 || card1 == card3 && card2 == card4 || card1 == card3 && card2 == card5 || card1 == card3 && card4 == card5 || card1 == card4 && card2 == card3 || card1 == card4 && card2 == card5 || card1 == card4 && card3 == card5 || card1 == card5 && card2 == card3 || card1 == card5 && card2 == card4 || card1 == card5 && card3 == card4 || card2 == card3 && card4 == card5 || card2 == card4 && card3 == card5 || card2 == card5 && card3 == card4){
        occur2Pair++; // if two sets of two cards equal each other, increment two pair +1
      }
      if (card1 == card2 || card1 == card3 || card1 == card4 || card1 == card5 || card2 == card3 || card2 == card4 || card2 == card5 || card3 == card4 || card3 == card5 || card4 == card5){
        occur1Pair++; // if two cards equal each other, increment one pair +1
      }
    }
    
    //Below calculates probability of each hand occuring based on collected data from code: (number of occurances) / (total hands drawn)
    double prob4Kind = occur4Kind / (double)numHands; 
    double prob3Kind = occur3Kind / (double)numHands;
    double prob2Pair = occur2Pair / (double)numHands;
    double prob1Pair = occur1Pair / (double)numHands; 
    
    //Below prints out number of hands and probabilities of each
    System.out.println("The number of loops: " + numHands);
    System.out.printf("The probability of Four-of-a-kind: %.3f %n", prob4Kind); 
    System.out.printf("The probability of Three-of-a-kind: %.3f %n", prob3Kind);
    System.out.printf("The probability of Two-pair: %.3f %n", prob2Pair);
    System.out.printf("The probability of One-pair: %.3f %n", prob1Pair); 
  }
}