/* Brian Karp     hw 07- Methods
Professor Carr    CSE 002
10/29/18          
hw07.java will print back user inputted string and give the user options to modify that string in a menu
*/

import java.util.Scanner; // import scanner class

public class hw07{
  public static void main(String[]args){ // main method
    Scanner myScanner = new Scanner(System.in);
    String text = "Error"; // sets error in place for variables that will be input by user
    String option = "Error";
    System.out.println("Enter a sample text:"); 
    boolean correctInput = myScanner.hasNext();
    text = myScanner.nextLine(); // user input for text
    System.out.println("You entered: " + text); // outputs back what they input 
    
    boolean menu = true; 
    while (correctInput = true){ // after each choice is made for menu, prints menu again
      printMenu(menu); // calls menu method to print menu
      option = myScanner.next(); // awaits user choice
      if (option.equals("q")){ // if user chooses q, they quit program
        break;
      }
      if (option.equals("c")){ // if user chooses c, call method to count number of non whitespace characters
        System.out.println("Number of non-whitespace characters: " + getNumOfNonWSCharacters(text));
        continue;
      }
      
      if (option.equals("w")){ // if user chooses w, calls methods to count number of words
        System.out.println("Number of words: " + getNumOfWords(text));
        continue;
      }
   
      if (option.equals("f")){ // if user chooses f, calls method to find number of occurances of a sample word
        System.out.println("Enter a word or phrase to be found: ");
        String sample = myScanner.next();
        System.out.println(sample + " instances: " + findText(text, sample));
        continue;
      }
    
      if (option.equals("r")){ // if user chooses r, calls method to replace ! with .
        System.out.println("Edited text: " + replaceExclamation(text));
        continue;
      }
 
      if (option.equals("s")){ // if user chooses s, calls method to shorten double spaces to single spaces
        System.out.println("Edited text: " + shortenSpaces(text));
        continue;
      }
      else{
        correctInput = false; 
        while (correctInput == false) { // if input not accepred
         System.out.println("Incorrect input letter. Try again: "); // Prompts user to try again
         myScanner.next(); // waits for new user input 
          correctInput = myScanner.hasNext(); // sets boolean value for loop to recheck
          if (correctInput = true){ // if new input is true after first failed
            option = myScanner.next(); // sets new input to variable
            break;
          }
        }
        continue;
      }
    }// closes while loop
   
    
    
  } //ends main method
  
  
  public static void printMenu(boolean menu){ // method that prints menu options
    System.out.println("MENU");
    System.out.println("c - Number of non-whitespace characters"); 
    System.out.println("w - Number of words"); 
    System.out.println("f - Find text"); 
    System.out.println("r - Replace all !'s"); 
    System.out.println("s - Shorten spaces"); 
    System.out.println("q - Quit"); 
    System.out.println("Choose an option:");
    return;
  }
  
  public static int getNumOfNonWSCharacters(String text){ // method to count non WS characters
    int nonWSChar = text.replaceAll(" ", "").length();// takes legnth of text with removed spaces
    return nonWSChar;
    }
  
  public static int getNumOfWords(String text){ // method to count number of words
    int wordCount = 1; 
    for (int i = 0; i < text.length(); i++){
      if (Character.isLetter(text.charAt(i))){ // if character is letter, keep going
        continue;
      }
      else if (!Character.isLetter(text.charAt(i))){ // if character is space increase word count
        wordCount++;
      }
      else if (Character.isLetter(text.charAt(i))) { // if character is letter after space increase word count
        wordCount++;
      }
    }
    return wordCount;
  }
  
  public static int findText(String text, String sample){ // method to count number of occurances of sample word
    int numSample = 0;
    if (text.indexOf(sample) != -1){ // if sample matches text, assigns numSample to return value of indexOf divided by length of the string
      numSample = (text.indexOf(sample) / sample.length());
    }
    return numSample;
  }
  
  public static String replaceExclamation(String text){ // method to print new text that replaces ! with .
    String newText = text.replaceAll("!", ".");
    return newText;
  }
  
  public static String shortenSpaces(String text){ // method to print new text that replaces double space with single space
    String newText = text.replaceAll("  ", " ");
    return newText;
  }
  
} // ends class
    