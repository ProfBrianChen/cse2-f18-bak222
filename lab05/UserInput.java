// Brian Karp   CSE 002
// 10/5/18      lab05
/* UserInput will ask for the course number, department name, the number of times it meets in a week, 
   the time the class starts, the instructor name, and the number of students. 
   
   It will check to make sure that what the user enters is actually of the correct type, 
   and if not, use an infinite loop to ask again until correct input is given. 
   */

import java.util.Scanner; // import scanner class 

public class UserInput{
  public static void main(String[]args){ // Java main method
    
// Below declares and initializes variables to cath errors if they are not changed with user input by end of code 
int courseNum = 0; 
int timesMeet = 0;
int numStudents = 0; 
String deptName = "Error";
String time = "Error";
String instructorName = "Error";
    
    Scanner myScanner = new Scanner(System.in); //declares new scanner 
    
System.out.println("Enter course number: "); // Prompts user to input number
Boolean correctInt = myScanner.hasNextInt(); // sets boolean value to check if the input is an integer or not 
if (correctInt == true) { // if input is integer 
courseNum = myScanner.nextInt(); // sets course number to input
} 
while (correctInt == false) { // if input not an integer 
	System.out.println("Incorrect course number. Try again: "); // Prompts user to try again
  correctInt = myScanner.hasNextInt(); // sets boolean value for loop to recheck
	myScanner.next(); // waits for new user input 
  if (correctInt = true){ // if new input is true after first failed
    courseNum = myScanner.nextInt(); // sets new input to variable
  }
} 

System.out.println("Enter department name: ");
Boolean correctString = myScanner.hasNext(); // sets scanner to check if String is inputed correctly 
if (correctString == true) { // if input is string 
deptName = myScanner.next(); // sets deptartment name to intput 
} 
while (correctString == false) { // if input is not string
	System.out.println("Incorrect department name. Try again: "); // prompts user to try again
  correctString = myScanner.hasNext(); // sets boolean value for loop to recheck
	myScanner.next(); // waits for new input
  if (correctString = true){ // if new input is true after first failed
    deptName = myScanner.next(); // sets new input to variable
  }
} 
    
System.out.println("Enter the number of times your class meets in a week: "); //prompts user to input
correctInt = myScanner.hasNextInt(); // sets scanner to check if String is inputed correctly 
if (correctInt == true) { // if input is integer 
timesMeet = myScanner.nextInt(); //sets times class meets in a week to input
} 
while (correctInt == false) { // if input is not integer 
	System.out.println("Incorrect number of times your class meets in a week. Try again: "); // prompts user of failed input
  correctInt = myScanner.hasNextInt(); // sets boolean value for loop to recheck
	myScanner.next(); // waits for new input
  if (correctInt = true){ // if new input is true after first failed
    timesMeet = myScanner.nextInt(); // sets new input to variable
  } 
}
    
System.out.println("Enter the time your class meets: "); //prompts user
correctString = myScanner.hasNext(); //sets variable to check if input is string
if (correctString == true) { // if string is valid
time = myScanner.next(); // sets time to input
} 
while (correctString == false) { // if string is not accepted 
	System.out.println("Incorrect time. Try again: "); // prompts user of failed input 
  correctString = myScanner.hasNext(); // sets boolean value for loop to recheck
	myScanner.next(); // waits for new input 
  if (correctString = true){ // if new input is true after first failed
    time = myScanner.next(); // sets new input to variable
  }
} 
    
System.out.println("Enter the instructor's name: "); // prompts user
correctString = myScanner.hasNext(); // prepares scanner to check if input is string 
if (correctString == true) { // if string is valid 
instructorName = myScanner.next(); // sets instructor name to string input
} 
while (correctString == false) { // if input is not string
	System.out.println("Incorrect instructor's name. Try again: "); // prompts user 
  correctString = myScanner.hasNext(); // sets boolean value for loop to recheck
	myScanner.next(); // waits for user input
  if (correctString = true){ // if new input is true after first failed
    instructorName = myScanner.next(); // sets new input to variable
  }
} 

System.out.println("Enter the number of students: "); // prompts user 
correctInt = myScanner.hasNextInt(); // sets boolean value for if statement to check if valid 
if (correctInt == true) { // if input is correct integer 
numStudents = myScanner.nextInt(); // sets number of students to input
} 
while (correctInt == false) { // if input not intger
	System.out.println("Incorrect number of students. Try again: "); // prompts to enter again
  correctInt = myScanner.hasNextInt(); // sets boolean value for loop to recheck
	myScanner.next(); // waits for user input 
  if (correctInt = true){ // if new input is true after first failed
    numStudents = myScanner.nextInt(); // sets new input to variable
  }
} 
    
System.out.println("Course number: " + courseNum + ". Department name: " + deptName + ". Meeting " + timesMeet + " times a week at " + time + ". Instructor's name: " + instructorName + ". Class size: " + numStudents);
// above is final print statement of all information input by user
    
      }
    }