/* Brian Karp         CSE 002
   hw04               9/22/18
   
   CrapsIf.java will print the slang terminology for craps rolls given either random or inputted dice
   */

import java.util.Scanner; // Imports scanner 

public class CrapsIf{
  public static void main(String args[]){
    //Java main method
    
    System.out.println("If you would like to choose your dice values, please enter one now."); 
    System.out.println("Or if you would like random numbers, please enter 0.");
    // Prompts user to input dice value or accept random ones
    
    Scanner myScanner = new Scanner (System.in); // Accepts scanner input
    int die1 = myScanner.nextInt(); // sets variable die 1 to next user input
    int randDie1 = (int) Math.ceil(Math.random() * 6);
    int randDie2 = (int) Math.ceil(Math.random() * 6); // Declares random values for dice if needed 
    String rollName = "Error"; 
   
    // Below will either accept user input of dice value or show two random values
    
      if (die1 > 6) {
      System.out.println("This is not a valid dice value."); // Inputs over 6 is invalid 
    } else if (die1 > 0) {
      System.out.println("Please enter your second die value:"); // Accepted die 1 prompts user for second die value 
      int die2 = myScanner.nextInt(); // sets variable die 2 to next user input
          if (die2 > 6) {
            System.out.println("This is not a valid dice value."); // Input over 6 is invalid 
      } else if (die2 == 0) {
        System.out.println("This is not a valid dice value."); // Input of 0 is invalid
      } else if (die2 > 0) { 
        // If die 2 input is good, then moves on to all possible rolls for user input
        
          if (die1 == 1 && die2 == 1) {
          rollName = "Snake Eyes"; // Snake Eyes: 1,1 
        } else if (die1 == 1 && die2 == 2) {
          rollName = "Ace Deuce"; // Ace Deuce 1,2
        } else if (die1 == 1 && die2 == 3) {
          rollName = "Easy Four"; // Easy Four 1,3
        } else if (die1 == 1 && die2 == 4) {
            rollName = "Fever Five"; // Fever Five 1,4
        } else if (die1 == 1 && die2 == 5) {
            rollName = "Easy Six"; // Easy Six 1,5
        } else if (die1 == 1 && die2 == 6) {
          rollName = "Seven Out"; // Seven Out 1,6
        } else if (die1 == 2 && die2 == 1) {
          rollName = "Ace Deuce"; // Ace Deuce 2,1
        } else if (die1 == 2 && die2 == 2) {
          rollName = "Hard Four"; // Hard Four 2,2
        } else if (die1 == 2 && die2 == 3) {
          rollName = "Fever Five"; // Fever Five 2,3
        } else if (die1 == 2 && die2 == 4) {
          rollName = "Easy Six"; // Easy Six 2,4
        } else if (die1 == 2 && die2 == 5) {
          rollName = "Seven Out"; // Seven Out 2,5
        } else if (die1 == 2 && die2 == 6) {
          rollName = "Easy Eight"; // Easy Eight 2,6
        } else if (die1 == 3 && die2 == 1) {
          rollName = "Easy Four"; // Easy Four 3,1
        } else if (die1 == 3 && die2 == 2) {
          rollName = "Fever Five"; // Fever Five 3,2
        } else if (die1 == 3 && die2 == 3) {
          rollName = "Hard Six"; // Hard Six 3,3
        } else if (die1 == 3 && die2 == 4) {
          rollName = "Seven Out"; // Seven Out 3,4
        } else if (die1 == 3 && die2 == 5) {
          rollName = "Easy Eight"; // Easy Eight 3,5
        } else if (die1 == 3 && die2 == 6) {
          rollName = "Nine"; // Nine 3,6
        } else if (die1 == 4 && die2 == 1) {
          rollName = "Fever Five"; // Fever Five 4,1
        } else if (die1 == 4 && die2 == 2) {
          rollName = "Easy Six"; // Easy Six 4,2
        } else if (die1 == 4 && die2 == 3) {
          rollName = "Seven Out"; // Seven Out 4,3
        } else if (die1 == 4 && die2 == 4) {
          rollName = "Hard Eight"; // Hard Eight 4,4
        } else if (die1 == 4 && die2 == 5) {
          rollName = "Nine"; // Nine 4,5
        } else if (die1 == 4 && die2 == 6) {
          rollName = "Easy Ten"; // Easy Ten 4,6
        } else if (die1 == 5 && die2 == 1) {
          rollName = "Easy Six"; // Easy Six 5,1
        } else if (die1 == 5 && die2 == 2) {
          rollName = "Seven Out"; // Seven Out 5,2
        } else if (die1 == 5 && die2 == 3) {
          rollName = "Easy Eight"; // Easy Eight 5,3 
        } else if (die1 == 5 && die2 == 4) {
          rollName = "Nine"; // Nine 5,4
        } else if (die1 == 5 && die2 == 5) {
          rollName = "Hard Ten"; // Hard Ten 5,5
        } else if (die1 == 5 && die2 == 6) {
          rollName = "Yo-leven"; // Yo-leven 5,6
        } else if (die1 == 6 && die2 == 1) {
          rollName = "Seven Out"; // Seven Out 6,1
        } else if (die1 == 6 && die2 == 2) {
          rollName = "Easy Eight"; // Easy Eight 6,2
        } else if (die1 == 6 && die2 == 3) {
          rollName = "Nine"; // Nine 6,3
        } else if (die1 == 6 && die2 == 4) {
          rollName = "Easy Ten"; // Easy Ten 6,4
        } else if (die1 == 6 && die2 == 5) {
          rollName = "Yo-leven"; // Yo-leven 6,5
        } else if (die1 == 6 && die2 == 6) {
          rollName = "Boxcars"; // Boxcars 6,6
        }
        System.out.println("Your roll of " + die1 + " and " + die2 + " is called: " + rollName); // Final output for specific rolls
      }  
      } else if (die1 == 0) { // Input of 0 initiates random roll 
        // Below lists all possible slang for random rolls 
        
          if (randDie1 == 1 && randDie2 == 1) {
          rollName = "Snake Eyes"; // Snake Eyes: 1,1 
        } else if (randDie1 == 1 && randDie2 == 2) {
          rollName = "Ace Deuce"; // Ace Deuce 1,2
        } else if (randDie1 == 1 && randDie2 == 3) {
          rollName = "Easy Four"; // Easy Four 1,3
        } else if (randDie1 == 1 && randDie2 == 4) {
            rollName = "Fever Five"; // Fever Five 1,4
        } else if (randDie1 == 1 && randDie2 == 5) {
            rollName = "Easy Six"; // Easy Six 1,5
        } else if (randDie1 == 1 && randDie2 == 6) {
          rollName = "Seven Out"; // Seven Out 1,6
        } else if (randDie1 == 2 && randDie2 == 1) {
          rollName = "Ace Deuce"; // Ace Deuce 2,1
        } else if (randDie1 == 2 && randDie2 == 2) {
          rollName = "Hard Four"; // Hard Four 2,2
        } else if (randDie1 == 2 && randDie2 == 3) {
          rollName = "Fever Five"; // Fever Five 2,3
        } else if (randDie1 == 2 && randDie2 == 4) {
          rollName = "Easy Six"; // Easy Six 2,4
        } else if (randDie1 == 2 && randDie2 == 5) {
          rollName = "Seven Out"; // Seven Out 2,5
        } else if (randDie1 == 2 && randDie2 == 6) {
          rollName = "Easy Eight"; // Easy Eight 2,6
        } else if (randDie1 == 3 && randDie2 == 1) {
          rollName = "Easy Four"; // Easy Four 3,1
        } else if (randDie1 == 3 && randDie2 == 2) {
          rollName = "Fever Five"; // Fever Five 3,2
        } else if (randDie1 == 3 && randDie2 == 3) {
          rollName = "Hard Six"; // Hard Six 3,3
        } else if (randDie1 == 3 && randDie2 == 4) {
          rollName = "Seven Out"; // Seven Out 3,4
        } else if (randDie1 == 3 && randDie2 == 5) {
          rollName = "Easy Eight"; // Easy Eight 3,5
        } else if (randDie1 == 3 && randDie2 == 6) {
          rollName = "Nine"; // Nine 3,6
        } else if (randDie1 == 4 && randDie2 == 1) {
          rollName = "Fever Five"; // Fever Five 4,1
        } else if (randDie1 == 4 && randDie2 == 2) {
          rollName = "Easy Six"; // Easy Six 4,2
        } else if (randDie1 == 4 && randDie2 == 3) {
          rollName = "Seven Out"; // Seven Out 4,3
        } else if (randDie1 == 4 && randDie2 == 4) {
          rollName = "Hard Eight"; // Hard Eight 4,4
        } else if (randDie1 == 4 && randDie2 == 5) {
          rollName = "Nine"; // Nine 4,5
        } else if (randDie1 == 4 && randDie2 == 6) {
          rollName = "Easy Ten"; // Easy Ten 4,6
        } else if (randDie1 == 5 && randDie2 == 1) {
          rollName = "Easy Six"; // Easy Six 5,1
        } else if (randDie1 == 5 && randDie2 == 2) {
          rollName = "Seven Out"; // Seven Out 5,2
        } else if (randDie1 == 5 && randDie2 == 3) {
          rollName = "Easy Eight"; // Easy Eight 5,3 
        } else if (randDie1 == 5 && randDie2 == 4) {
          rollName = "Nine"; // Nine 5,4
        } else if (randDie1 == 5 && randDie2 == 5) {
          rollName = "Hard Ten"; // Hard Ten 5,5
        } else if (randDie1 == 5 && randDie2 == 6) {
          rollName = "Yo-leven"; // Yo-leven 5,6
        } else if (randDie1 == 6 && randDie2 == 1) {
          rollName = "Seven Out"; // Seven Out 6,1
        } else if (randDie1 == 6 && randDie2 == 2) {
          rollName = "Easy Eight"; // Easy Eight 6,2
        } else if (randDie1 == 6 && randDie2 == 3) {
          rollName = "Nine"; // Nine 6,3
        } else if (randDie1 == 6 && randDie2 == 4) {
          rollName = "Easy Ten"; // Easy Ten 6,4
        } else if (randDie1 == 6 && randDie2 == 5) {
          rollName = "Yo-leven"; // Yo-leven 6,5
        } else if (randDie1 == 6 && randDie2 == 6) {
          rollName = "Boxcars"; // Boxcars 6,6
        }
           System.out.println("Your random dice roll is " + randDie1 + " and " + randDie2 + " and it is called: " + rollName);  
      } // Closes random roll 'if' statements/ Above is final output for random roll
    
    } 
  } // Ends program