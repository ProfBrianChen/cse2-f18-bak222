/* Brian Karp          CSE 002 
 hw04- Craps Switch   9/23/18
 
 CrapsSwitch will print the slang terminology for dice rolls given inputted values or random numbers
 */

import java.util.Scanner; // Imports scanner class

public class CrapsSwitch{
  public static void main(String args[]){
    // Java main method above
    
    Scanner myScanner = new Scanner (System.in);
    
    System.out.println("Please enter a die value or enter 0 if you want a random roll:"); // Tells user to enter value of die or choose random
    int die1 = myScanner.nextInt(); // Declares varaibles die 1 and 2
    int die2;
    
    int randDie1 = (int) Math.ceil(Math.random() * 6); // Declares and initializes random die rolls if needed 
    int randDie2 = (int) Math.ceil(Math.random() * 6);
    
    String rollName = "Unknown";
    
    switch (die1) {
      case 0: // Random Roll\
        switch (randDie1) {
          case 1: // Die 1 = 1
            switch (randDie2) {
            case 1: // Die 2 = 1
            rollName = "Snake Eyes";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break;
            
            case 2: // Die 2 = 2
            rollName = "Ace Deuce";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break; 
            
            case 3: // Die 2 = 3
            rollName = "Easy Four";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break; 
            
          case 4: // Die 2 = 4
            rollName = "Fever Five";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break;
            
          case 5: // Die 2 = 5
            rollName = "Easy Six";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break;
            
          case 6: // Die 2 = 6
            rollName = "Seven Out";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break;     
            
          default: 
            System.out.println("Invalid input.");
            break;
        }
        break;
        
      case 2: // Die 1 = 2
        switch (randDie2) {
            case 1: // Die 2 = 1
            rollName = "Ace Deuce";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break;
            
          case 2: // Die 2 = 2
            rollName = "Hard Four";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break; 
            
          case 3: // Die 2 = 3
            rollName = "Fever Five";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break; 
            
          case 4: // Die 2 = 4
            rollName = "Easy Six";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break;
            
          case 5: // Die 2 = 5
            rollName = "Seven Out";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break;
            
          case 6: // Die 2 = 6
            rollName = "Easy Eight";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break;     
            
          default: 
            System.out.println("Invalid input.");
            break;
        }
        break; 
        
      case 3: // Die 1 = 3
        switch (randDie2) {
            case 1: // Die 2 = 1
            rollName = "Easy Four";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break;
            
          case 2: // Die 2 = 2
            rollName = "Fever Five";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break; 
            
          case 3: // Die 2 = 3
            rollName = "Hard Six";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break; 
            
          case 4: // Die 2 = 4
            rollName = "Seven Out";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break;
            
          case 5: // Die 2 = 5
            rollName = "Easy Eight";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break;
            
          case 6: // Die 2 = 6
            rollName = "Nine";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break;     
            
          default: 
            System.out.println("Invalid input.");
            break;
        }
        break;
        
      case 4: // Die 1 = 4
        switch (randDie2) {
            case 1: // Die 2 = 1
            rollName = "Fever Five";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break;
            
          case 2: // Die 2 = 2
            rollName = "Easy Six";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break; 
            
          case 3: // Die 2 = 3
            rollName = "Seven Out";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break; 
            
          case 4: // Die 2 = 4
            rollName = "Hard Eight";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break;
            
          case 5: // Die 2 = 5
            rollName = "Nine";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break;
            
          case 6: // Die 2 = 6
            rollName = "Easy Ten";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break;     
            
          default: 
            System.out.println("Invalid input.");
            break;
        }
        break;
        
      case 5: // Die 1 = 5 
        switch (randDie2) {
            case 1: // Die 2 = 1
            rollName = "Easy Six";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break;
            
          case 2: // Die 2 = 2
            rollName = "Seven Out";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break; 
            
          case 3: // Die 2 = 3
            rollName = "Easy Eight";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break; 
            
          case 4: // Die 2 = 4
            rollName = "Nine";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break;
            
          case 5: // Die 2 = 5
            rollName = "Hard Ten";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break;
            
          case 6: // Die 2 = 6
            rollName = "Yo-leven";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break;     
            
          default: 
            System.out.println("Invalid input.");
            break;
        }
        break;
        
      case 6: // Die 1 = 6
        switch (randDie2) {
            case 1: // Die 2 = 1
            rollName = "Seven Out";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break;
            
          case 2: // Die 2 = 2
            rollName = "Easy Eight";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break; 
            
          case 3: // Die 2 = 3
            rollName = "Nine";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break; 
            
          case 4: // Die 2 = 4
            rollName = "Easy Ten";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break;
            
          case 5: // Die 2 = 5
            rollName = "Yo-Leven";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break;
            
          case 6: // Die 2 = 6
            rollName = "Boxcars";
            System.out.println("Your roll of " + randDie1 + " " + randDie2 + " is called: " + rollName);
            break;     
            
          default: 
            System.out.println("Invalid input.");
            break;
        } 
        break; 
        } // End of random die cases
        break; 
        
      // Below cases are die1 input and within each case (1-6) each craps roll is named in relation to following die2 input  
      case 1: // Die 1 = 1
        System.out.println("Please enter the second die value: "); // Prompts user to put in second value
        die2 = myScanner.nextInt(); // Accepts user input for die 2
        switch (die2) {
          case 1: // Die 2 = 1
            rollName = "Snake Eyes";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break;
            
          case 2: // Die 2 = 2
            rollName = "Ace Deuce";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break; 
            
          case 3: // Die 2 = 3
            rollName = "Easy Four";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break; 
            
          case 4: // Die 2 = 4
            rollName = "Fever Five";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break;
            
          case 5: // Die 2 = 5
            rollName = "Easy Six";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break;
            
          case 6: // Die 2 = 6
            rollName = "Seven Out";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break;     
            
          default: 
            System.out.println("Invalid input.");
            break;
        }
        break;
        
      case 2: // Die 1 = 2
        System.out.println("Please enter the second die value: ");
        die2 = myScanner.nextInt();
        switch (die2) {
            case 1: // Die 2 = 1
            rollName = "Ace Deuce";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break;
            
          case 2: // Die 2 = 2
            rollName = "Hard Four";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break; 
            
          case 3: // Die 2 = 3
            rollName = "Fever Five";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break; 
            
          case 4: // Die 2 = 4
            rollName = "Easy Six";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break;
            
          case 5: // Die 2 = 5
            rollName = "Seven Out";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break;
            
          case 6: // Die 2 = 6
            rollName = "Easy Eight";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break;     
            
          default: 
            System.out.println("Invalid input.");
            break;
        }
        break; 
        
      case 3: // Die 1 = 3
        System.out.println("Please enter the second die value: ");
        die2 = myScanner.nextInt();
        switch (die2) {
            case 1: // Die 2 = 1
            rollName = "Easy Four";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break;
            
          case 2: // Die 2 = 2
            rollName = "Fever Five";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break; 
            
          case 3: // Die 2 = 3
            rollName = "Hard Six";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break; 
            
          case 4: // Die 2 = 4
            rollName = "Seven Out";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break;
            
          case 5: // Die 2 = 5
            rollName = "Easy Eight";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break;
            
          case 6: // Die 2 = 6
            rollName = "Nine";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break;     
            
          default: 
            System.out.println("Invalid input.");
            break;
        }
        break;
        
      case 4: // Die 1 = 4
        System.out.println("Please enter the second die value: ");
        die2 = myScanner.nextInt();
        switch (die2) {
            case 1: // Die 2 = 1
            rollName = "Fever Five";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break;
            
          case 2: // Die 2 = 2
            rollName = "Easy Six";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break; 
            
          case 3: // Die 2 = 3
            rollName = "Seven Out";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break; 
            
          case 4: // Die 2 = 4
            rollName = "Hard Eight";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break;
            
          case 5: // Die 2 = 5
            rollName = "Nine";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break;
            
          case 6: // Die 2 = 6
            rollName = "Easy Ten";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break;     
            
          default: 
            System.out.println("Invalid input.");
            break;
        }
        break;
        
      case 5: // Die 1 = 5 
        System.out.println("Please enter the second die value: ");
        die2 = myScanner.nextInt();
        switch (die2) {
            case 1: // Die 2 = 1
            rollName = "Easy Six";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break;
            
          case 2: // Die 2 = 2
            rollName = "Seven Out";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break; 
            
          case 3: // Die 2 = 3
            rollName = "Easy Eight";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break; 
            
          case 4: // Die 2 = 4
            rollName = "Nine";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break;
            
          case 5: // Die 2 = 5
            rollName = "Hard Ten";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break;
            
          case 6: // Die 2 = 6
            rollName = "Yo-leven";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break;     
            
          default: 
            System.out.println("Invalid input.");
            break;
        }
        break;
        
      case 6: // Die 1 = 6
        System.out.println("Please enter the second die value: ");
        die2 = myScanner.nextInt();
        switch (die2) {
            case 1: // Die 2 = 1
            rollName = "Seven Out";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break;
            
          case 2: // Die 2 = 2
            rollName = "Easy Eight";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break; 
            
          case 3: // Die 2 = 3
            rollName = "Nine";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break; 
            
          case 4: // Die 2 = 4
            rollName = "Easy Ten";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break;
            
          case 5: // Die 2 = 5
            rollName = "Yo-Leven";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break;
            
          case 6: // Die 2 = 6
            rollName = "Boxcars";
            System.out.println("Your roll of " + die1 + " " + die2 + " is called: " + rollName);
            break;     
            
          default: 
            System.out.println("Invalid input.");
            break;
        }
        break;
        
      default:
        System.out.println("Invalid input."); // Outputs invalid if not 1-6 
        break;
    } // Ends outermost switch statement
    
    
  }
} // Ends program