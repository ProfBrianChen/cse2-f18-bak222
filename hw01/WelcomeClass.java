 /* CSE 02 Welcome Class
      hw01
      Brian Karp
     */

public class WelcomeClass{  
  
  public static void main(String args[]){
    //Prints Welcome message to terminal
    
System.out.println("    -----------");
System.out.println("    | WELCOME |");
System.out.println("    -----------");
System.out.println("  ^  ^  ^  ^  ^  ^");
System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
System.out.println("<-b--a--k--2--2--2->");
System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
System.out.println("  v  v  v  v  v  v");
System.out.println(" My name is Brian Karp and I'm from Princeton, New Jersey.");
System.out.println(" My favorite pie might be apple pie.");
     }              
}      
                   