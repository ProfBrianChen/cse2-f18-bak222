 /* CSE 02 Arithmetic Calculations
      hw02
      Brian Karp
     */

public class ArithmeticCalculations{  
  
  public static void main(String args[]){

//Number of pairs of pants
int numPants = 3;
//Cost per pair of pants
double pantsPrice = 34.98;

//Number of sweatshirts
int numShirts = 2;
//Cost per shirt
double shirtPrice = 24.99;

//Number of belts
int numBelts = 1;
//cost per belt
double beltCost = 33.99;

//the tax rate
double paSalesTax = 0.06;

double totalCostOfPants;   //total cost of pants
    totalCostOfPants = numPants * pantsPrice;
double totalCostOfShirts; //total cost of shirts
    totalCostOfShirts = numShirts * shirtPrice;
double totalCostOfBelt; //total cost of belts
    totalCostOfBelt = numBelts * beltCost;
    
double salesTaxOnPants; //total sales tax on pants
    salesTaxOnPants = paSalesTax * totalCostOfPants;
double salesTaxOnShirts; //total sales tax on shirts
    salesTaxOnShirts = paSalesTax * totalCostOfShirts;
double salesTaxOnBelts; //total sales tax on belts
    salesTaxOnBelts = paSalesTax * totalCostOfBelt;

double totalCostPreTax; //total cost of purchases before tax
    totalCostPreTax = totalCostOfBelt + totalCostOfShirts + totalCostOfPants;
double totalSalesTax; //total sales tax
    totalSalesTax = salesTaxOnBelts + salesTaxOnShirts + salesTaxOnPants;
double totalCost; //total cost after tax
    totalCost = totalCostPreTax + totalSalesTax;

System.out.println("The total cost pre tax is " + totalCostPreTax);
    System.out.println("The total cost with tax is " + totalCost);
    
  }
}
