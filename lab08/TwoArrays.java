/* Brian Karp
CSE 002
Professor Carr
11/9/18
Arrays.java will create two arrays, one with random numbers, and one that counts instances of each number in the first array 
*/

import java.util.Random;
import java.util.Arrays; // imports necessary stuff 

public class TwoArrays{
  public static void main(String[]args){
    
    int count = 0; // variables
    int currentInt = 0;
    
    int[] arrayRandom = new int[100];// establishes new array of 100 random numbers 0-99
    for (int i = 0; i < arrayRandom.length; i++){
      arrayRandom[i] = (int)(Math.random() * 100);

      int[] arrayCount = new int[100]; // new array to count instances of other numbers in first array
      currentInt = arrayRandom[i]; // sets temp variable to current random generated variable
      count = 0; // resets count

      for (int j = 0; j < arrayRandom.length; j++){ // for if temp var = random var in array, count increases, goes through whole random array
        if (currentInt == arrayRandom[j]){
          count++;
        }
      }
      System.out.println(currentInt + " occurs " + count + "time(s)"); // prints which numbers hold instances in random array
   }                                                                      // otherwise not shown means no instances
     
    
    System.out.println(arrayRandom.toString()); 
    String str = Arrays.toString(arrayRandom); 
    System.out.println("Array 1 holds the following integers: " + str); // prints random array
    
  }
}