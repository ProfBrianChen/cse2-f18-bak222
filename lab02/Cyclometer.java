/* 
Brian Karp   9/7/18
lab02        CSE 002

  Cyclometer will measure the number of minutes, counts, 
   distance of each trip in miles for each trip and 
   the distance for the two trips combined
*/

public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
      
  int secsTrip1=480;  // Declares the number of seconds as an integer Trip1 takes
  int secsTrip2=3220;  // Declares the number of seconds as an integer Trip2 takes 
	int countsTrip1=1561; // Declares the number of counts as an interger Trip1 takes
	int countsTrip2=9037; // Declares the number of counts as an interger Trip2 takes
      
    double wheelDiameter=27.0,  // Declares diameter of wheel as double
  	PI=3.14159, // Declares what PI is equal to 
  	feetPerMile=5280,  // Declares how many feet are in a mile 
  	inchesPerFoot=12,   // Declares how many inches per foot
  	secondsPerMinute=60;  // Declares how many seconds per minutes
	  double distanceTrip1, distanceTrip2,totalDistance; // Declares double variables of distance for trips 1 and 2 and total distance

      System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	    System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");
      //Above prints how many minutes each trip took
      
      distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	/* (for each count, a rotation of the wheel travels
    	the diameter in inches times PI) */
	distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles for Trip1
	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; // Gives distance in miles for Trip2
	totalDistance=distanceTrip1+distanceTrip2; // Gives total distance of combined trips
      
      // Below prints out the output data.
  System.out.println("Trip 1 was "+distanceTrip1+" miles");
	System.out.println("Trip 2 was "+distanceTrip2+" miles");
	System.out.println("The total distance was "+totalDistance+" miles");

	}  //end of main method   
} //end of class
