/* Brian Karp
Professor Carr
CSE 002
11/13/18
HW8.java will shiffle a deck of cards and select a hand*/

import java.util.Scanner;
import java.util.Arrays;
import java.util.Random;
public class HW8{ 
  public static void main(String[] args) { 
    Scanner scan = new Scanner(System.in); 
 //suits club, heart, spade or diamond 
    String[] suitNames = {"C","H","S","D"};    
    String[] rankNames = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
    String[] cards = new String[52]; 
    String[] hand = new String[5]; 
    System.out.println("Enter how big you want your hand: "); // asks for hand size
    int numCards = scan.nextInt(); //scanner accepts input
    while(numCards > 52){
      System.out.println("Hand too big, Try again: ");
      numCards = scan.nextInt();
    }
    int again = 1; 
    int index = 51;
    for (int i = 0; i < 52; i++){ 
      cards[i] = rankNames[i%13] + suitNames[i/13]; 
      System.out.print(cards[i] + " "); 
    } 
    System.out.println(); 
    shuffle(cards); 
    printArray(cards); 
    while(again == 1){ // loop to recieve more hands
       System.out.println("Hand");
       hand = getHand(cards,index,numCards); 
       printArray(hand);
       index = index - numCards;
       System.out.println("Enter a 1 if you want another hand drawn"); 
       while(numCards >= 53){
         System.out.println("Hand too big, Try again: ");
         numCards = scan.nextInt();
       }
       again = scan.nextInt(); 
    }  
  }
    
  public static void shuffle(String[] list){ // method to shuffle cards 
    Random rand = new Random(); // random number gen
    System.out.println("Shuffled");
    for (int i = 0; i < list.length; i++) { // loop each card into random spot
      int randomPosition = rand.nextInt(list.length);
      String temp = list[i]; // temp var to hold place
      list[i] = list[randomPosition]; // random position move
      list[randomPosition] = temp; // into temp varable again
    }
  }
  
  public static String[] getHand(String[] list, int index, int numCards){ //method to get new hand
    String[] hand = new String[numCards];
    if(index >= 0 && numCards <= 52){
      for(int i = 0; i < hand.length; i++){//loops next hand from top of deck
        hand[i] = list[index]; // takes from index down to indicate top of deck
        index--;
      }
    }
    return hand;
  }
  
  public static void printArray(String[] list){ //method to print array
    System.out.println(list.toString()); //convert array to string
    String str = Arrays.toString(list); //
    System.out.println(str); // print
  }    
} 