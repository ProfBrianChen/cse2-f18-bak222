/* Brian Karp
CSE 002
Professor Carr
11/16/18
lab09 will pass arrays as method arguments
*/
import java.util.Scanner;
import java.util.Arrays;
public class lab09{
  public static void main(String[] args){  
    Scanner myScanner = new Scanner (System.in);
    int[] array0 = new int[8];
    System.out.println("Enter 8 integers: ");
    for(int i = 0; i < array0.length; i++){
      array0[i] = myScanner.nextInt();
    } // Above sets first array with user input
    int[] array1 = copy(array0);
    int[] array2 = copy(array0);
    inverter(array0); // inverts array0
    print(array0); // prints array0
    
    inverter2(array0); // inverts array1
    print(array1); // prints array1
    
    int[] array3 = inverter2(array2); // sets array3 to inverted array2
    print(array3); // prints above
  }
  
  public static int[] copy(int[] array0){ // method copies input array
    int[] newArray = new int[array0.length];
    for(int i = 0; i < newArray.length; i++){
    	newArray[i] = array0[i];
    }
    return newArray;
  }
    
  public static void inverter(int[] array0){ // method inverts input array
    for(int i = 0; i < (array0.length / 2); ++i){
      int temp = array0[i];
    	array0[i] = array0[(array0.length - 1) - i];
      array0[(array0.length - 1) - i] = temp;
    }
  } 
  
  public static int[] inverter2(int[] array1){ //method inverts copy of array
    copy(array1);
    for(int i = 0; i < (array1.length / 2); ++i){
      int temp = array1[i];
    	array1[i] = array1[(array1.length - 1) - i];
      array1[(array1.length - 1) - i] = temp;
    }
    return array1;
  } 
  
  public static void print(int[] array){ //prints array
    for(int i = 0; i < array.length; i++){
      System.out.print(array[i] + " ");
    }
    System.out.println();
  }
  

}