/* Brian Karp       CSE 002 
   HW06- EncryptedX  Professor Carr
   10/22/18
   
Encrypted X will print a grid of asteriks of the size according to user input from 0-100 with the letter X shown by spaces between characters
   */

import java.util.Scanner;

public class EncryptedX{
  public static void main(String[]args){
  
    Scanner myScanner = new Scanner(System.in);
    
    System.out.println("Enter an integer from 0 to 100");
    boolean correctInt = myScanner.hasNextInt();
    int size = 0;
    
    if (correctInt == true && size < 100){ // if input is integer and within range, assigns variable 
      size = myScanner.nextInt(); 
    }
    
    while (correctInt == false) { // while input is not intger, prints error 
      System.out.println("Incorrect integer. Try again: "); 
      correctInt = myScanner.hasNextInt(); // checks again
      myScanner.next(); // waits for next input 
      if (correctInt == true && size < 100){ // if input is accepted, assign variable size  
        size = myScanner.nextInt(); // assigns size to input
      }
    }
    while (size > 100) { // while input out of range, prints error 
      System.out.println("Incorrect integer. Try again: "); 
      correctInt = myScanner.hasNextInt(); // checks if input is integer 
      size = myScanner.nextInt(); // assigns size to input    
      if (correctInt == true && size < 100){ // if input is accepted, move on
        continue;
      }
    }
    
    for (int row = 1; row <= size; row++){ // sets loop for size of X, counts rows with sentinel row
      if (row == 1){ // prints first space for first row 
        System.out.print(" ");
      }
      if (row == size){ // prints first space for last row
        System.out.print(" ");
      }
      for (int counter = 1; counter < size; counter++){ // sets loop to prints asteriks by size of input
        if (counter == row && counter != 1){ // prints a space at the place same number as row number 
          System.out.print(" ");              // ex: second row prints space after 1 asterik, third row prints space after 2 asteriks etc.
        }
        System.out.print("*");  // prints asteriks in loop
        if (counter == size - row){ // prints a second space per row relative to total size - number of row, symmetrical to first space
          System.out.print(" ");
        }   
      } 
      System.out.println(); // prints new line for next row 
      if (row == (size / 2)){ // when we reach middle of even sized pattern
        for (int counter = 1; counter < size; counter++){ // loops to print asteriks 
          System.out.print("*");
          if (counter == (size / 2) && (size % 2) == 0){ // middle of middle row gets 1 space and extra asterik when EVEN input
            System.out.print(" *"); 
          }
          if (counter == (size / 2) && (size % 2) != 0){ // middle of middle row gets 2 space when ODD input
            System.out.print("  "); 
          }
        }
        System.out.println(); // prints new line for next row 
      }
    }
    
    
    
    
  }
}