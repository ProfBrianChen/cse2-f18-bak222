/* Brian Karp     CSE 002
hw03-Pyramid      9/18/18

Pyramid will calculate the volume of a pyramid given the dimentions of the base length, width, and height.
*/
import java.util.Scanner; //imports scanner

public class Pyramid{
  public static void main(String[] args) {
    //Java main method required 
    
    Scanner myScanner = new Scanner ( System.in ); 
    //accepts input and declares new scanner 
      
    
    System.out.print("Enter the length of a pyramid:");
      //prompts user to input length
    double length = myScanner.nextDouble (); 
      //accepts user input of length
   
    System.out.println("Enter the width of a pyramid:");
      //promts user to input width
    double width = myScanner.nextDouble ();
      //accepts user input of width
    
    System.out.println("Enter the height of a pyramid:");
      //promts user to input height
    double height = myScanner.nextDouble (); 
      //accepts user input of height

    double volPyramid = (length * width * height) / 3;
      //calculates volume of a pyramid
    
    System.out.println("The volume of your pyramid is " + volPyramid);
    //prints to screen what the volume of the pyramid is
    
    
  }
}