/* Brian Karp     CSE 002
hw03-Convert      9/18/18

Convert will convert the quantity of rain to cubic miles given the acres of land and inches of rain
*/

import java.util.Scanner; //imports scanner

public class Convert{
  public static void main(String[] args) {
    //Required Java main method
    
    Scanner myScanner = new Scanner ( System.in ); //accepts input of scanner and declares new scanner 
    
System.out.print("Enter the afflicted area in acres: "); //prints instructions to user to enter acres of land affected 
  double acres = myScanner.nextDouble ();
    //accepts user input of acres
  
System.out.print("Enter the average inches of rainfall: "); //prints instructions to user to enter inches of rain
  double inches = myScanner.nextDouble ();
    //accepts user input of inches of rain
    
    double acreInches = acres * inches; //declares acre inches variable as double and calculates by multiplying acres and inches 
    double gallons = acreInches * 27154; //declares variable of cubic miles and calculates it from inches input
    double cubicMiles = gallons * (9.08169 * Math.pow(10,-13)); //declares cubic miles variable as double and calculates it

System.out.println(cubicMiles + " cubic miles."); //prints the cubic miles of affected 
  }
}