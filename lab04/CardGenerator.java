// Brian Karp   CSE 002
// lab 4        Card Generator 

// Card Generator will select a random card from a deck sorted by suit

import java.util.Random; // imports random number generator 

public class CardGenerator{
  public static void main(String args[]){
    // Java main method
    
    Random randGen = new Random ();
    // Prepares for random number 
    
    int cardNum = randGen.nextInt(13) + 1; // declares and initializes card number as integer and finds random number from 1-13
    int suitNum = randGen.nextInt(4) + 1; // delcares and initializes random number to assign 1 of 4 suits 
    String suit; // delcares suit as string variable 
    String cardName; // delcares card name variable for final number or face cards 
    
    // Below assigns Jack face values 
    if (cardNum == 11) {
      cardName = "Jack";
    } else if (cardNum == 12) {
      cardName = "Queen";
    } else if (cardNum == 13) {
      cardName = "King";
    } else if (cardNum == 1) {
      cardName = "Ace";
    } else {
      cardName = Integer.toString(cardNum);
   }
    
    // Below assigns random number 1-4 a suit value
    if (suitNum == 1) {
      suit = "Hearts";
    } else if (suitNum == 2) {
      suit = "Diamonds";
    } else if (suitNum == 3) {
      suit = "Clubs"; 
    } else {
      suit = "Spades";
    }
      
 System.out.println("You picked the " + cardName + " of " + suit);  
    // Prints out random card you picked
  }
}