/* Brian Karp      CSE 002
* lab06        Pattern D
* 10/16/18     Professor Carr

Pattern D will display a certain pattern through the use of nested loops after asking for user to input the legnth of the pattern by rows
*/

import java.util.Scanner; // import scanner 

public class PatternD{
  public static void main(String[]args){ // Java main method
    
    int patternSize = 0; // declares integer size to indicate how big of a pattern
    Scanner myScanner = new Scanner(System.in);
    
    System.out.println("Input the size of the pattern with an integer from 1 to 10: ");
    boolean correctInt = myScanner.hasNextInt(); // assigns boolean for if statement condition, must be integer 1-10
    
    if (correctInt == true){ // if true
      patternSize = myScanner.nextInt(); // assigns pattern size to input if is integer
    } 
    while (correctInt == false) { // if false
      System.out.println("Incorrect integer, try again: "); // prompts error if not integer 
      correctInt = myScanner.hasNextInt(); // checks if input is integer 
      myScanner.next(); // waits for user input
      if (correctInt == true){ // if true 
        patternSize = myScanner.nextInt(); // assigns pattern size to integer after initial failure 
      }
    }
    
    for(int numRows = patternSize; numRows >= 1; numRows--){ // determines size of the pattern
      for(int counter = numRows; counter >= 1; counter--){ // uses counter to decrement based on row number printing decreasing numbers 
        System.out.print(counter + " ");
      }
      System.out.println(); // prints new line after a loop of counter prints
    }
    
  }
}

