/* Brian Karp      CSE 002
* lab06        Pattern A
* 10/16/18     Professor Carr

Pattern A will display a certain pattern through the use of nested loops after asking for user to input the legnth of the pattern by rows
*/

import java.util.Scanner; // imports scanner class

public class PatternA{
  public static void main(String[]args){ // Java main method
    
    int patternSize = 0; // declares integer size to indicate how big of a pattern
    Scanner myScanner = new Scanner(System.in);
    
    System.out.println("Input the size of the pattern with an integer from 1 to 10: ");
    boolean correctInt = myScanner.hasNextInt(); // assigns boolean to check if input is integer 
    
    if (correctInt == true){ // if true
      patternSize = myScanner.nextInt(); // assigns pattern size to input if is integer
    }  
    while (correctInt == false) { // if incorrect input (not int or out of range) 
      System.out.println("Incorrect integer, try again: "); 
      correctInt = myScanner.hasNextInt(); // checks if input is integer 
      myScanner.next(); // waits for user input    
      if (correctInt == true){ // if true after initial failure 
        patternSize = myScanner.nextInt(); // assigns pattern size to integer 
      }
    }
    
    for(int numRows = 1; numRows <= patternSize; numRows++){ // determines size of the pattern by counting up number of rows to match input
      for(int counter = 1; counter <= numRows; counter++){ // uses counter to print numbers incrementing from 1 until reaches number of rows
        System.out.print(counter + " ");
      }
      System.out.println(); // prints new line for pattern 
    }
    
  }
}
